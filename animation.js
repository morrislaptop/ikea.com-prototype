// document.observe('dom:loaded', function () {
// 	slider.options.slide = false;

// 	setTimeout(function() {
// 		slider.options.currentSlide = 2;
// 		slider.options.slide = true;
// 	}, 5000)
// });

// Object.extend(com.ikea.irw.homepage.carousel, {
// 	callOnintervals: function () {
// 		console.log('i got control now!');
//         if (this.options.slide) {
//             this.moveNext();
//             this.options.timeoutEvent = setTimeout(this.callOnintervals.bind(this), this.options.speed * 1000);
//         }
//     }
// });
$namespace('com.ikea.irw');

com.ikea.irw.robotocat = (function () {

	var robotocat = document.querySelector("#robotocat");
	var publicFunctions = {};

	publicFunctions.init = function() {
		var img = attachImage();
		attachEvents(img);
	}

	function attachImage() {
		var img = document.createElement("img");
		img.src = "robot.png";
		img.className = 'robotocat';
		src = document.querySelector("#allContent");
		src.appendChild(img);

		return img;
	}

	function attachEvents(img) {
		img.addEventListener('webkitAnimationEnd', function (e) {
			stopSlider();
		}, false);
	}

	function stopSlider() {
		console.log('taking control');
		com.ikea.irw.homepage.carousel.prototype.moveNext = function () {
			console.log('no move next!');
		};
	}
	
    return publicFunctions;

}());